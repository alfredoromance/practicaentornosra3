/*
 * Clase para refactorizar
 * Codificacion UTF-8
 * Paquete: ra3.refactorizacion
 * Se deben comentar todas las refactorizaciones realizadas,
 * mediante comentarios de una linea o de bloque.
 */

package refactorizacion;

import java.util.Scanner;

/** 
 * @author fvaldeon
 * @since 31-01-2018
 */

//El nombre de la clase debe ser en mayuscula

public class Refact {
	
	//Se cambia el nombre porque "cad" no nos deice nada, ademas es una constante
	final static String SALUDO = "Bienvenido al programa";

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//El Scanner no puede llamarse c
		
		Scanner input = new Scanner(System.in);
		
		System.out.println(SALUDO);
		
		System.out.println("Introduce tu dni");
		//El mensaje del syso nos ayuda a dar nombre a la variable
		String DNI = input.nextLine();
		System.out.println("Introduce tu nombre");
		String nombre = input.nextLine();
	
		/*
		 * Las variables deben  escribirse una en cada linea
		 * por lo tanto pondremos dos variables int y dos variables String
		 * ademas en los tipos int no deberiamos llamarlas a y b si no de otra forma, por ejemplo
		 * numeroA y numeroB
		 * 
		 * Y deben declarar al momento de uso, y son magic numbers
		 */
	
		int numeroA=7; 
		int numeroB=16;
		int numeroC=25;
		
		//Debemos escribir correctamente en los if, es decir poniendo el parentesis cuando es necesario
		if(numeroA > numeroB || (numeroC % 5) != 0 && ((numeroC * 3) - 1) > (numeroB / numeroC)){
			
			System.out.println("Se cumple la condición");
		}
		
		//Escribir el parentesis para diferenciar las operaciones que se realizan primero
		numeroC = numeroA + (numeroB * numeroC) + (numeroB / numeroA);
		
		/*
		 * La declaracion correcta de una array deberia ser String[] array
		 * los corchetes siempre se colocan entre su tipo de dato y el nombre que le damos
		 * 
		 * Y al ser un array de los dias de la semana y que su valor no va a cambiar se deben escribir
		 * todos en un mismo corchete
		 * 
		 *  El nombre del Array no dice nada por eso debemos cambiarlo a algo mas apropiado
		 */
		
		String[] arrayDiasSemana = {"Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado", "Domingo"};
		
		//El nombre del metodo tampoco nos dice a que se refiere con exactitud
		
		mostrarDiasSemana(arrayDiasSemana);
		
	}
	//De nuevo los corchetes estan mal situados y tambien las llaves, el comiendo de una llave 
	//Siempre debe estar detras de un parentesis
	static void mostrarDiasSemana(String[] vectorDeStrings){
		
		//La variable del for debe ser una letra unica y en vez de poner un numero podemos poner la variable.length
		for(int i = 0; i < vectorDeStrings.length; i++) {
			
			System.out.println("El dia de la semana en el que"
			
					//Aqui debemos poner la i
					+ "te encuentras [" + (i + 1) +" -7] es el dia: " 
			
					+ vectorDeStrings[i]);
		}
	}
}


