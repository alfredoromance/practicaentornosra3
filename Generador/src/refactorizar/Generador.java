package refactorizar;

import java.util.Scanner;

public class Generador {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		/**
		 * el mennu
		 */
		int longitud = menu(scanner);
		/**
		 * 2 opcion
		 */
		int opcion = opcion(scanner);
		
		String password = "";
		password = Switch(longitud, opcion, password);

		System.out.println(password);
		scanner.close();
	}

	private static String Switch(int longitud, int opcion, String password) {
		switch (opcion) {
		case 1:
			
			password = caractereAZ(longitud, password);
			break;
		case 2:
			password = numeroszeronine(longitud, password);
			break;
		case 3:
			password = letrasycaracterE(longitud, password);
			break;
		case 4:
			password = letrasnumCaraEsp(longitud, password);
			break;
		}
		return password;
	}

	/**
	 * @param longitud
	 * @param password
	 * @return
	 */
	public static String letrasnumCaraEsp(int longitud, String password) {
		for (int i = 0; i < longitud; i++) {
			int n;
			n = (int) (Math.random() * 3);
			if (n == 1) {				
				char letra4 = (char) ((Math.random() * 26) + 65);
				password += letra4;
			} else if (n == 2) {
			
			char caracter4 = (char) ((Math.random() * 15) + 33);
				password += caracter4;
			} else {
				
			int	numero4 = (int) (Math.random() * 10);
				password += numero4;
			}
		}
		return password;
	}

	/**
	 * @param longitud
	 * @param password
	 * @return
	 */
	public static String letrasycaracterE(int longitud, String password) {
		for (int i = 0; i < longitud; i++) {
			int n;
			n = (int) (Math.random() * 2);
			if (n == 1) {
				
			char	letra3 = (char) ((Math.random() * 26) + 65);
				password += letra3;
			} else {
		
			char	caracter3 = (char) ((Math.random() * 15) + 33);
				password += caracter3;
			}
		}
		return password;
	}

	/**
	 * @param longitud
	 * @param password
	 * @return
	 */
	public static String numeroszeronine(int longitud, String password) {
		for (int i = 0; i < longitud; i++) {
		
		int	numero2 = (int) (Math.random() * 10);
			password += numero2;
		}
		return password;
	}

	/**
	 * @param longitud
	 * @param password
	 * @return
	 */
	public static String caractereAZ(int longitud, String password) {
		for (int i = 0; i < longitud; i++) {
		
		char letra1 = (char) ((Math.random() * 26) + 65);
			password += letra1;
		}
		return password;
	}

	/**
	 * @param scanner
	 * @return
	 */
	public static int opcion(Scanner scanner) {
		System.out.println("Elige tipo de password: ");
		int opcion = scanner.nextInt();
		return opcion;
	}

	/**
	 * @param scanner
	 * @return
	 */
	public static int menu(Scanner scanner) {
		System.out.println("Programa que genera passwords de la longitud indicada, y del rango de caracteres");
		System.out.println("1 - Caracteres desde A - Z");
		System.out.println("2 - Numeros del 0 al 9");
		System.out.println("3 - Letras y caracteres especiales");
		System.out.println("4 - Letras, numeros y caracteres especiales");
		System.out.println("Introduce la longitud de la cadena: ");
		int longitud = scanner.nextInt();
		return longitud;
	}

}

