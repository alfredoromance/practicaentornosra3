package  refactorizacion;

/*
 * Clase para refactorizar
 * Codificacion UTF-8
 * Paquete: ra3.refactorizacion
 * Se deben comentar todas las refactorizaciones realizadas,
 * mediante comentarios de una linea o de bloque.
 */

import java.util.Scanner;
/**
 * 
 * @author fvaldeon
 * @since 31-01-2018
 */
public class Refactor {

	// Un escaner nunca sera una final static
			
	public static void main(String[] args) 
	{
	
		// Esta mal declarado 
		Scanner input = new Scanner(System.in);
		
		
		//borrar variable n
		
		
		
		int cantidad_maxima_alumnos;
		
		cantidad_maxima_alumnos = 10;
		
		//array  mal declarado
		
		int[] arraysNotas = new int[10];
		
		//falta int bucle for a la variable del int "i"
	
		
		for(int i = 0; i < 10; i++)
		{
			//escribir  scanner bien
			
			System.out.println("Introduce nota media de alumno");
			arraysNotas[i] = input.nextInt();//poner nextint porque es un array de numbers
		}	
		
		System.out.println("El resultado es: " + recorrer_array(arraysNotas));
		
		input.close();
	}
	//El corchete mal puesto
	static double recorrer_array(int vector[]){
		//no llamar variables con letra
		double numero = 0;
		
		//variable for poner i
		for(int i = 0; i < 10; i++) 
		{
			numero = numero + vector[i];
		}
		return numero/10;
	}
	
}